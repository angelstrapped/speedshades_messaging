#!python3

class Message:
    ''' Build a message object out of an IRC-like TMI message:
        :sender!sender@sender.tmi.twitch.tv PRIVMSG #channel :message '''
    def __init__(self, message=":sender! PRIVMSG #channel :message"):
        self.message = ":".join(message.split(":")[2:])
        self.sender = message.split("!")[0][1:]
        self.channel = message.split(" ")[2]

class MessageWithSenderPrivileges(Message):
    ''' Message object with additional information about
        sender's privileges in the channel. '''
    def __init__(
        self,
        message=":sender! PRIVMSG #channel :message",
        sender_is_moderator=False,
        sender_is_channel_owner=False
    ):
        super().__init__(message)
        self.sender_is_moderator = sender_is_moderator
        self.sender_is_channel_owner = sender_is_channel_owner

class Response:
    ''' Response string with consumption indicator. '''
    def __init__(self, consumed=False, message=None):
        self.consumed = consumed
        self.message = message

class LastMessage:
    ''' Set and retrieve the last message sent to chat. '''
    def __init__(self):
        self.lastmessage = {}
    
    def set(self, reply, channel):
        # Sets the last message sent on a per-channel basis.
        self.lastmessage[channel] = reply
    
    def get(self, channel):
        # Returns the last message sent to the specified channel.
        if channel in self.lastmessage:
            return self.lastmessage[channel]
        else:
            return None

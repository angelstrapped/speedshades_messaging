from distutils.core import setup

setup(
    name='speedshades-messaging',
    version='0.1.0',
    py_modules=['speedshadesmessaging']
)

